subroutine sieve(is_prime, n_max)
! =====================================================
! Uses the sieve of Eratosthenes to compute a logical
! array of size n_max, where .true. in element i
! indicates that i is a prime.
! =====================================================
   integer, intent(in)   :: n_max
   logical, intent(out)  :: is_prime(n_max)
   integer :: i
   is_prime = .true.
   is_prime(1) = .false.
   do i = 2, int(sqrt(real(n_max)))
      if (is_prime(i)) is_prime(i*i:n_max:i) = .false.
   end do
   return
end subroutine

subroutine kmeans(centroid_assignement, centroids, data, K, iteration_num)
! =====================================================
! K-means is an iterative algorithm that groups
!  N -dimensional data into K groups.
! =====================================================
   implicit none
   ! input
   integer, intent(in)   :: K,  iteration_num
   !real, intent(in)   :: data(measurementsN, num_feat) ! should be a big numpy table with N elements, each has N features
   real, intent(in)   :: data(:,:) ! should be a big numpy table with N elements, each has N features
   ! output
   !integer :: measurementsN
   !integer :: num_feat
   ! measurementsN = size(data,1)
   num_feat = size(data,2)
   !integer, intent(out)  :: centroid_assignement(measurementsN)
   integer, intent(out)  :: centroid_assignement(size(data,1))
   real, intent(out) :: centroids(K, size(data,2))
   !external vector_distance_func
   ! initially random centroid assignment
   ! todo: current implemetation takes first K elements,
   !       future one should take K random elements
   integer :: i, j, l
   real :: testarray(k, num_feat)
   real :: test_vector(num_feat)
   real vector_distance_func ! function
   integer closest_neighbour_ind ! function
   centroids(1:k, 1:num_feat) = data(1:k, 1:num_feat)
   forall (j=1:iteration_num)
      forall (i=1:measurementsN)
         centroid_assignement(i) = closest_neighbour_ind(centroids, K, num_feat, data(i, :))
      end forall
      forall (l=1:k)
      end forall
   end forall
   do l = 1, k
      call get_centroids(data, test_vector, k, num_feat)
      centroids(l, :) = test_vector
   end do
   print *, centroid_assignement
   print *, findloc(centroid_assignement, 7)
   !print *,findloc(transpose(centroid_assignement(1:1,:)),7)
  ! print *, centroid_assignement(1:2, 1:1)
end subroutine

function closest_neighbour_ind(centroids, K, N, in_point) result(centroid_assignement)
   ! input
   integer, intent(in)   :: K, N
   real, intent(in)   :: centroids(K, N)
   real, intent(in)   :: in_point(N)
   ! output
   integer  :: centroid_assignement
   ! local variables
   integer :: iter
   real :: dist
   real :: dist2
   ! external functions
   real vector_distance_func
   ! function body
   dist = vector_distance_func(centroids(1, :), in_point, N)
   centroid_assignement = 1
   do iter = 2, K
      dist2 = vector_distance_func(centroids(iter, :), in_point, N)
      if (dist2 < dist) then
         dist2 = dist
         centroid_assignement = iter
      end if
   end do
end function closest_neighbour_ind

subroutine get_centroids(vin, vout, ni, nj)
   implicit none
   integer(4), intent(in) :: ni, nj
   real(4), dimension(ni, nj), intent(in) :: vin
   real(4), dimension(nj), intent(out) :: vout
   integer(4) :: i
   do i = 1, ni
      vout = vout + vin(i, :)
   end do
   vout = vout/ni
end subroutine get_centroids

pure real function vector_distance_func(arr_x, arr_y, L)
   implicit none
   ! input
   integer, intent(in)   :: L
   real, intent(in)   :: arr_x(L)
   real, intent(in)   :: arr_y(L)
   !external vector_distance
   ! output
   !real :: res
   !call vector_distance(res, arr_x, arr_y, L)
   !vector_distance_func = res
   vector_distance_func = sqrt(sum((arr_x - arr_y)**2))
end function vector_distance_func
subroutine vector_distance(D, arr_x, arr_y, L)
   implicit none
   ! input
   integer, intent(in)   :: L
   real, intent(in)   :: arr_x(L)
   real, intent(in)   :: arr_y(L)
   ! output
   real, intent(out)   :: D
   ! classical formula for euclidean distance
   ! actually fortran has already an intrinsic procedure
   ! for this: norm2
   D = sqrt(sum((arr_x - arr_y)**2))
end subroutine

subroutine doubling3(vin, vout, ni, nj)
   implicit none
   integer(4), intent(in) :: ni, nj
   real(4), dimension(ni, nj), intent(in) :: vin
   real(4), dimension(ni, nj), intent(out) :: vout
   integer(4) :: i, j

   do j = 1, nj
      do i = 1, ni
         vout(i, j) = vin(i, j)*2
      end do
   end do
end subroutine doubling3

subroutine logical_to_integer(prime_numbers, is_prime, num_primes, n)
! =====================================================
! Translates the logical array from sieve to an array
! of size num_primes of prime numbers.
! =====================================================
   integer                 :: i, j = 0
   integer, intent(in)     :: n
   logical, intent(in)     :: is_prime(n)
   integer, intent(in)     :: num_primes
   integer, intent(out)    :: prime_numbers(num_primes)
   do i = 1, size(is_prime)
      if (is_prime(i)) then
         j = j + 1
         prime_numbers(j) = i
      end if
   end do
end subroutine
